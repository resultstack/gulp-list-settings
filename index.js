'use strict';

const path = require('path');
const PluginError = require('plugin-error');
const through = require('through2');
const Vinyl = require('vinyl');
const pluginName = 'gulp-list-settings';

module.exports = function() {
  let firstFile;
  const settings = {};

  const scanRegex = /(?<=process\.env\.)([a-z0-9_]+)(?:\s*\|\|\s*([^&`,;!)}\]\r\n]+))?/ig;
  const scanString = str => {
    let match;
    while (match = scanRegex.exec(str)) {
      const name = match[1].replace(/APPSETTING_/i, '');
      if (settings[name]) continue;
      settings[name] = (match[2] || "");
    }
  }

  return through.obj(
    function(file, enc, cb) {
      if (file.isNull()) {
        cb(null, file);
        return;
      }

      if (file.isStream()) {
        cb(new PluginError(pluginName, 'Streaming not supported'))
        return;
      }

      if (/node_modules/i.test(file.path)) {
        cb(null, file);
        return;
      }

      firstFile = firstFile || file;

      if (!/\.[jt]s$/i.test(file.path))
        return cb(null, file);

      if (file.isBuffer())
        scanString(file.contents.toString());

      cb();
    },
    function(cb) {
      const names = Object.keys(settings).sort();
      const contents = names.map(n => settings[n].trim() ? n +' || '+ settings[n].trim() : n)
        .join('\n');

      const outputFile = new Vinyl({
        base: firstFile.base,
        contents: Buffer.from(contents),
        cwd: firstFile.cwd,
        path: path.join(firstFile.base, 'settings.txt'),
      });

      cb(null, outputFile);
    }
  );
}